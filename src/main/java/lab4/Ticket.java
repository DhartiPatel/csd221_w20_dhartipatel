/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

/**
 *
 * @author student
 */
public class Ticket extends CashTill implements SaleableItem {
    
    String description;
    double price;
    String client;

    @Override
    public String toString() {
        return "Ticket{" + "description=" + description + ", price=" + price + ", client=" + client + '}';
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }
    public Ticket(){
    
    }

    public Ticket(String description, double price, String client) {
        this.description = description;
        this.price = price;
        this.client = client;
    }

    @Override
    public void sellCopy() {
        System.out.println("**********************************************************");
        System.out.println("                   TICKET VOUCHER                         ");
        System.err.println(toString());
        System.out.println("**********************************************************");
        System.err.println("");
    }

    @Override
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
}
