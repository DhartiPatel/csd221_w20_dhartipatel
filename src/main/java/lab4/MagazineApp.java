package lab4;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class MagazineApp {
    private ArrayList<Magazine> magazine = new ArrayList<>();

    private int currentIndex = 0;
    private Scanner input = new Scanner(System.in);

    private String menu = ""
            + "1. List Magazine\n"
            + "2. Add a Magazine\n"
            + "3. Edit a Magazine\n"
            + "4. Delete a Magazine\n"
            + "5. Sell a Magazine\n"
            + "6. quit\n";

    public MagazineApp() {
    }

    public void run() {
        boolean quit = false;
        while (!quit) {
            System.out.println(menu);
            int choice = input.nextInt();
            switch (choice) {
            case 1:
                list();
                break;
            case 2: {
                try {
                    add();
                    list();
                } catch (Exception ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;
            case 3:
                edit();
                break;
            case 4:
                delete();
                break;
            case 5:
                sell();
                break;
            case 6:
                System.out.println("All Done.");
                quit = true;
                break;
            default:
                System.out.println("Invalid Input.");
            }

        }
    }

    private void add() throws Exception {
        try {
            Magazine b1 = new Magazine();
            System.out.println("---------------- Add a Book ------------");
            System.out.println("Quantity to order:");
            b1.setCopies(getInt());
            System.out.println("Title:");
            b1.setTitle(getString());
            System.out.println("Price");
            b1.setPrice(getDouble());
            System.out.println("Enter Order Qty:");
            b1.setOrderQty(getInt());
            System.out.println("Enter Current Issue:");
            b1.setCurrIssue(getString());

            magazine.add(b1);

        } catch (Exception e) {
            throw new Exception("Error Adding a book");
        }
    }

    private void list() {
        try {
            FileOutputStream saveFile = new FileOutputStream("MagazineList.sav");
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(magazine);
            save.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("-------------\n\n");
        System.out.println("\tTitle \tcopies \tprice \tOrderQty \tCurrentIssue\n");
        System.out.println("\t-------------------------------\n");
        ArrayList<Magazine> magazinesIn = null;
        try {
            FileInputStream saveFileIn = new FileInputStream("MagazineList.sav");
            ObjectInputStream restore = new ObjectInputStream(saveFileIn);
            magazinesIn = (ArrayList<Magazine>) restore.readObject();
            saveFileIn.close();
        } catch (Exception ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Magazine mag : magazinesIn) {
            if (mag == null) {
                break;
            }
            System.out.println("\t" + mag);
        }
    }

    public void edit() {
        list();
        System.out.println("Enter magazine number to edit detail");
        int no = input.nextInt();
        if (no - 1 > magazine.size() - 1) {
            System.out.println("Input Not valid.");
        } else {
            Magazine c3 = magazine.get(no - 1);

            System.out.println("Title: " + magazine.get(no - 1).getTitle());
            System.out.println("Enter Title if you want to edit otherwise hit enter: ");
            input = new Scanner(System.in);
            String title = input.nextLine();
            if (title.equals("")) {
                title = magazine.get(no - 1).getTitle();
            }
            c3.setTitle(title);

            System.out.println("Quantity: " + magazine.get(no - 1).getCopies());
            System.out.println("Enter Quantity if you want to edit otherwise hit enter:");
            String is = input.nextLine();
            int qty = 0;
            if (!is.isEmpty()) {
                Scanner in2 = new Scanner(is);
                try {
                    qty = in2.nextInt();
                } catch (java.util.InputMismatchException e) {
                    System.out.println("Sorry you did not enter a number");
                }
            } else {
                qty = magazine.get(no - 1).getCopies();
            }
            c3.setCopies(qty);
            
            System.out.println("Order Quantity: " + magazine.get(no - 1).getOrderQty());
            System.out.println("Enter Quantity if you want to edit otherwise hit enter:");
            String is1 = input.nextLine();
            int ordQty = 0;
            if (!is.isEmpty()) {
                Scanner in2 = new Scanner(is1);
                try {
                    ordQty = in2.nextInt();
                } catch (java.util.InputMismatchException e) {
                    System.out.println("Sorry you did not enter a number");
                }
            } else {
                qty = magazine.get(no - 1).getOrderQty();
            }
            c3.setOrderQty(ordQty);
            
            System.out.println("Title: " + magazine.get(no - 1).getTitle());
            System.out.println("Enter Title if you want to edit otherwise hit enter: ");
            input = new Scanner(System.in);
            String currIssue = input.nextLine();
            if (currIssue.equals("")) {
                currIssue = magazine.get(no - 1).getCurrIssue();
            }
            c3.setCurrIssue(currIssue);

            System.out.println("Price: " + magazine.get(no - 1).getPrice());
            System.out.println("Enter Price if you want to edit otherwise hit enter:");
            String s1 = input.nextLine();
            double price = 0;
            if (!is.isEmpty()) {
                Scanner in2 = new Scanner(s1);
                try {
                    price = in2.nextDouble();
                } catch (java.util.InputMismatchException e) {
                    System.out.println("Sorry you did not enter a number");
                }
            } else {
                price = magazine.get(no - 1).getPrice();
            }
            c3.setPrice(price);
        }
        list();
    }

    public void delete() {
        System.out.println("Enter book number to delete");
        int no = input.nextInt();
        if (no > magazine.size()) {
            System.out.println("Input Not valid.");
        } else {
            magazine.remove(no - 1);
        }
    }
    
    public void sell() {
        System.out.println("Enter book number to sell");
        int no = input.nextInt();
        if (no > magazine.size()) {
            System.out.println("Input Not valid.");
        } else {
            Publication p1= new Magazine();
            p1= magazine.get(no - 1);
            p1.sellCopy();
        }
    }

    private String getString() {
        input = new Scanner(System.in);
        String in = input.nextLine();
        if (!in.isEmpty()) {
            return in;
        }
        return null;
    }

    private int getInt() {
        boolean done = false;
        int i = 0;
        while (!done) {
            String in = input.nextLine();
            if (!in.isEmpty()) {
                Scanner in2 = new Scanner(in);
                try {
                    i = in2.nextInt();
                    done = true;
                } catch (Exception e) {
                    System.out.println("Sorry you did not enter a number");
//                    throw new Exception();
                }
            } else {
                System.out.println("Use default");
            }
        }
        return i;

    }

    private double getDouble() {
        boolean done = false;
        double i = 0;
        while (!done) {
            String in = input.nextLine();
            if (!in.isEmpty()) {
                Scanner in2 = new Scanner(in);
                try {
                    i = in2.nextDouble();
                    done = true;
                } catch (Exception e) {
                    System.out.println("Sorry you did not enter a number");
//                    throw new Exception();
                }
            } else {
                System.out.println("Use default");
            }
        }
        return i;

    }

}

