
package lab4;

import java.io.Serializable;


/**
 * @author student
 */

public abstract class Publication extends CashTill implements Serializable, SaleableItem {

    
    private Long id;

    
    private String title;

    
    private Double price;

 
    private int copies;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Publication{" + "id=" + id + ", title=" + title + ", price=" + price + ", copies=" + copies + '}';
    }

    public int getCopies() {
        return this.copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }
    
    public void sellCopy(){
        super.sellItem(this);
    }

}