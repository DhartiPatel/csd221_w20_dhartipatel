/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class App {
    
    private Scanner input = new Scanner(System.in);
    private String menu = ""
            + "1. Books\n"
            + "2. Magazines\n"
            + "3. Tickets\n"
            + "4. quit\n";

    boolean quit = false;

    public App() {
    }

    public void run() {

        while (!quit) {
            System.out.println(menu);
            int menuChoice = input.nextInt();
            switch (menuChoice) {
                case 1:
                    new BookApp().run();
                    break;
                case 2: 
                    new MagazineApp().run();
                    break;
                case 3:
                    new TicketApp().run();
                    break;
                case 4:
                System.out.println("All Done.");
                quit = true;
                break;
                default:
                    System.out.println("Invalid Input.");
            }

        }
    }
}
