
package lab4;

import java.io.Serializable;

/**
 * @author student
 */
public class Book extends Publication implements Serializable {

    private String author;

    public String getAuthor() {
        return this.author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    
    public String toString(){
        return author +"\t"+ super.getTitle()+"\t"+super.getCopies()+"\t"+super.getPrice();
    }

}