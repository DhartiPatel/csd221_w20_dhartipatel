/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab4;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class TicketApp {

    private Scanner input = new Scanner(System.in);

    private String ticketMenu = ""
            + "1. Sell a Ticket\n"
            + "2. Exit\n";
    
    boolean quitMainMnu = false;

    public TicketApp() {
    }

    public void run() {
        boolean quit = false;
        while (!quit) {
            System.out.println(ticketMenu);
            int choice = input.nextInt();
            switch (choice) {
            case 1:
                sell();
                break;
            case 2:
                System.out.println("All Done.");
                quit = true;
                break;
            default:
                System.out.println("Invalid Input.");
            }

        }
    }
    public void sell() {
            Ticket t1 = new Ticket();
            System.out.println("---------------- Ticket Detail ------------");
            System.out.println("Description:");
            t1.setDescription(getString());
            System.out.println("Price");
            t1.setPrice(getDouble());
            System.out.println("Client:");
            t1.setClient(getString());

            t1.sellCopy();

    }
    
    private String getString() {
        input = new Scanner(System.in);
        String in = input.nextLine();
        if (!in.isEmpty()) {
            return in;
        }
        return null;
    }

    private int getInt() {
        boolean done = false;
        int i = 0;
        while (!done) {
            String in = input.nextLine();
            if (!in.isEmpty()) {
                Scanner in2 = new Scanner(in);
                try {
                    i = in2.nextInt();
                    done = true;
                } catch (Exception e) {
                    System.out.println("Sorry you did not enter a number");
//                    throw new Exception();
                }
            } else {
                System.out.println("Use default");
            }
        }
        return i;

    }

    private double getDouble() {
        boolean done = false;
        double i = 0;
        while (!done) {
            String in = input.nextLine();
            if (!in.isEmpty()) {
                Scanner in2 = new Scanner(in);
                try {
                    i = in2.nextDouble();
                    done = true;
                } catch (Exception e) {
                    System.out.println("Sorry you did not enter a number");
//                    throw new Exception();
                }
            } else {
                System.out.println("Use default");
            }
        }
        return i;

    }
}
