
package lab4;

import java.io.Serializable;
import java.sql.Date;

/**
 * @author student
 */

public class Magazine extends Publication implements Serializable {

    
    private int orderQty;

    private String currIssue;

    public int getOrderQty() {
        return this.orderQty;
    }

    public void setOrderQty(int orderQty) {
        this.orderQty = orderQty;
    }

    public String getCurrIssue() {
        return this.currIssue;
    }

    public void setCurrIssue(String currIssue) {
        this.currIssue = currIssue;
    }
    

}