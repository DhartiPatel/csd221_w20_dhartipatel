/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab5.Application;

import lab4.*;

/**
 *
 * @author student
 */
public interface SaleableItem {
    public void sellCopy();
    public Double getPrice();
}
