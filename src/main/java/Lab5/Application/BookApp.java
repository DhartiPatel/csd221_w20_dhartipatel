/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab5.Application;

import lab4.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class BookApp {
    private ArrayList<Book> books = new ArrayList<>();

    private int currentIndex = 0;
    private Scanner input = new Scanner(System.in);

    private String menu = ""
            + "1. List Books\n"
            + "2. Add a Book\n"
            + "3. Edit a Book\n"
            + "4. Delete a Book\n"
            + "5. Sell a Book\n"
            + "6. quit\n";

    public BookApp() {
    }

    public void run() {
        boolean quit = false;
        while (!quit) {
            System.out.println(menu);
            int choice = input.nextInt();
            switch (choice) {
            case 1:
                list();
                break;
            case 2: {
                try {
                    add();
                    list();
                } catch (Exception ex) {
                    Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            break;
            case 3:
                edit();
                break;
            case 4:
                delete();
                break;
            case 5:
                sell();
                break;
            case 6:
                System.out.println("All Done.");
                quit = true;
                break;
            default:
                System.out.println("Invalid Input.");
        }

        }
    }

    private void add() throws Exception {
        try {
            Book b1 = new Book();
            System.out.println("---------------- Add a Book ------------");
            System.out.println("Enter Author:");
            b1.setAuthor(getString());
            System.out.println("Quantity to order:");
            b1.setCopies(getInt());
            System.out.println("Title:");
            b1.setTitle(getString());
            System.out.println("Price");
            b1.setPrice(getDouble());

            books.add(b1);

        } catch (Exception e) {
            throw new Exception("Error Adding a book");
        }
    }

    private void list() {
        try {
            FileOutputStream saveFile = new FileOutputStream("BookList.sav");
            ObjectOutputStream save = new ObjectOutputStream(saveFile);
            save.writeObject(books);
            save.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("-------------\n\n");
        System.out.println("\tAuthor \ttitle \tcopies \tprice\n");
        System.out.println("\t-------------------------------\n");
        ArrayList<Book> booksIn = null;
        try {
            FileInputStream saveFileIn = new FileInputStream("BookList.sav");
            ObjectInputStream restore = new ObjectInputStream(saveFileIn);
            booksIn = (ArrayList<Book>) restore.readObject();
            saveFileIn.close();
        } catch (Exception ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (Book book : booksIn) {
            if (book == null) {
                break;
            }
            System.out.println("\t" + book);
        }
    }

    public void edit() {
        list();
        System.out.println("Enter Book number to edit detail");
        int no = input.nextInt();
        if (no - 1 > books.size() - 1) {
            System.out.println("Input Not valid.");
        } else {
            Book c3 = books.get(no - 1);
            System.out.println("Author: " + books.get(no - 1).getAuthor());
            System.out.println("Enter Title if you want to edit otherwise hit enter: ");
            input = new Scanner(System.in);
            String author = input.nextLine();
            if (author.equals("")) {
                author = books.get(no - 1).getAuthor();
            }
            c3.setAuthor(author);

            System.out.println("Title: " + books.get(no - 1).getTitle());
            System.out.println("Enter Title if you want to edit otherwise hit enter: ");
            input = new Scanner(System.in);
            String title = input.nextLine();
            if (title.equals("")) {
                title = books.get(no - 1).getTitle();
            }
            c3.setTitle(title);

            System.out.println("Quantity: " + books.get(no - 1).getCopies());
            System.out.println("Enter Quantity if you want to edit otherwise hit enter:");
            String is = input.nextLine();
            int qty = 0;
            if (!is.isEmpty()) {
                Scanner in2 = new Scanner(is);
                try {
                    qty = in2.nextInt();
                } catch (java.util.InputMismatchException e) {
                    System.out.println("Sorry you did not enter a number");
                }
            } else {
                qty = books.get(no - 1).getCopies();
            }
            c3.setCopies(qty);

            System.out.println("Price: " + books.get(no - 1).getPrice());
            System.out.println("Enter Price if you want to edit otherwise hit enter:");
            String s1 = input.nextLine();
            double price = 0;
            if (!is.isEmpty()) {
                Scanner in2 = new Scanner(s1);
                try {
                    price = in2.nextDouble();
                } catch (java.util.InputMismatchException e) {
                    System.out.println("Sorry you did not enter a number");
                }
            } else {
                price = books.get(no - 1).getPrice();
            }
            c3.setPrice(price);
        }
        list();
    }

    public void delete() {
        System.out.println("Enter book number to delete");
        int no = input.nextInt();
        if (no > books.size()) {
            System.out.println("Input Not valid.");
        } else {
            books.remove(no - 1);
        }
    }
    public void sell() {
        System.out.println("Enter book number to sell");
        int no = input.nextInt();
        if (no > books.size()) {
            System.out.println("Input Not valid.");
        } else {
            Publication p1= new Book();
            p1= books.get(no - 1);
            p1.sellCopy();
        }
    }

    private String getString() {
        input = new Scanner(System.in);
        String in = input.nextLine();
        if (!in.isEmpty()) {
            return in;
        }
        return null;
    }

    private int getInt() {
        boolean done = false;
        int i = 0;
        while (!done) {
            String in = input.nextLine();
            if (!in.isEmpty()) {
                Scanner in2 = new Scanner(in);
                try {
                    i = in2.nextInt();
                    done = true;
                } catch (Exception e) {
                    System.out.println("Sorry you did not enter a number");
//                    throw new Exception();
                }
            } else {
                System.out.println("Use default");
            }
        }
        return i;

    }

    private double getDouble() {
        boolean done = false;
        double i = 0;
        while (!done) {
            String in = input.nextLine();
            if (!in.isEmpty()) {
                Scanner in2 = new Scanner(in);
                try {
                    i = in2.nextDouble();
                    done = true;
                } catch (Exception e) {
                    System.out.println("Sorry you did not enter a number");
//                    throw new Exception();
                }
            } else {
                System.out.println("Use default");
            }
        }
        return i;

    }

}

