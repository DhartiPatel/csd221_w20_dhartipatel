/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab3;

import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author student
 */
public class AppJPA {

    private static Scanner input = new Scanner(System.in);
    private static EntityManagerFactory emf = null;
    private static EntityManager em = null;
    private static List<Book> ListOfPublication;
    
    public AppJPA(){
        
    }

    public void run() {

        int choice = 0;
        boolean done = false;

        try {
            emf = Persistence.createEntityManagerFactory("DEFAULT_PU");
            em = emf.createEntityManager();
            Logger.getLogger(Lab2.q5.Main.class.getName()).log(Level.INFO, "Entity Manager created (" + emf + ")");
            while (!done) {
                try {
                    System.out.println("1. List all Books");
                    System.out.println("2. Option 2");
                    System.out.println("3. Add a book");
                    System.out.println("4. Edit a book");
                    System.out.println("5. Delete a book");
                    System.out.println("6. Quit");
                    System.out.println("Enter your choice");
                    choice = input.nextInt();

                    switch (choice) {
                        case 1:
                            listBook();
                            break;
                        case 2:
                            //doOption2();
                            break;
                        case 3:
                            addBook();
                            break;
                        case 4:
                            editBook();
                            break;
                        case 5:
                            deleteBook();
                            break;
                        case 6:
                            done = true;
                            break;
                        default:
                            System.out.println("Wrong entry try again.");
                    }
                } catch (Exception e) {
                    System.out.println("Wrong entry try again."+e.getMessage());
                    input.next();
                }
            }

        } catch (Exception e) {
            Logger.getLogger(lab3.AppJPA.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (emf != null) {
                emf.close();
            }

        }
    }

    private static void listBook() {
        emf = Persistence.createEntityManagerFactory("DEFAULT_PU");
        em = emf.createEntityManager();
        ListOfPublication = em.createQuery("SELECT c FROM Book c").getResultList();
        System.out.println("List of Books");
        for (Publication customer : ListOfPublication) {
            System.out.println(customer.getTitle());
        }
    }

    private static void addBook() throws Exception {
        Book c = new Book();
        try {
            Scanner input = new Scanner(System.in);
            System.out.println("---------------- Add a Book ------------");
            System.out.println("Enter Author('q' to quit):");
            c.setTitle(input.next());
            System.out.println("Quantity to order:");
            c.setCopies(input.nextInt());
            System.out.println("Title:");
            c.setTitle(input.next());
            System.out.println("Price");
            c.setPrice(input.nextDouble());

            em.persist(c);

            em.getTransaction().commit();

        } catch (Exception e) {
            throw new Exception("Error Adding a Book");
        }

    }

    private static void editBook() {
        listBook();
        System.out.println("Enter Car number to edit detail");
        int no = input.nextInt();
        ListOfPublication = em.createQuery("SELECT c FROM Book c").getResultList();

        Book c3 = ListOfPublication.get(no - 1);
        if (no - 1 > ListOfPublication.size() - 1) {
            System.out.println("Input Not valid.");
        } else {
            em.getTransaction().begin();

            System.out.println("Author: " + ListOfPublication.get(no - 1).getAuthor());
            System.out.println("Enter Title if you want to edit otherwise hit enter: ");
            input = new Scanner(System.in);
            String author = input.nextLine();
            if (author.equals("")) {
                author = ListOfPublication.get(no - 1).getAuthor();
            }
            c3.setAuthor(author);

            System.out.println("Title: " + ListOfPublication.get(no - 1).getTitle());
            System.out.println("Enter Title if you want to edit otherwise hit enter: ");
            input = new Scanner(System.in);
            String title = input.nextLine();
            if (title.equals("")) {
                title = ListOfPublication.get(no - 1).getTitle();
            }
            c3.setTitle(title);

            System.out.println("Quantity: " + ListOfPublication.get(no - 1).getCopies());
            System.out.println("Enter Quantity if you want to edit otherwise hit enter:");
            String is = input.nextLine();
            int qty = 0;
            if (!is.isEmpty()) {
                Scanner in2 = new Scanner(is);
                try {
                    qty = in2.nextInt();
                } catch (java.util.InputMismatchException e) {
                    System.out.println("Sorry you did not enter a number");
                }
            } else {
                qty = ListOfPublication.get(no - 1).getCopies();
            }
            c3.setCopies(qty);

            System.out.println("Price: " + ListOfPublication.get(no - 1).getPrice());
            System.out.println("Enter Price if you want to edit otherwise hit enter:");
            String s1 = input.nextLine();
            double price = 0;
            if (!is.isEmpty()) {
                Scanner in2 = new Scanner(s1);
                try {
                    price = in2.nextDouble();
                } catch (java.util.InputMismatchException e) {
                    System.out.println("Sorry you did not enter a number");
                }
            } else {
                price = ListOfPublication.get(no - 1).getPrice();
            }
            c3.setPrice(price);

            em.merge(c3);
            em.getTransaction().commit();
        }
        listBook();
    }

    private static void deleteBook() {
        System.out.println("Enter Car number to edit detail");
        int no = input.nextInt();
        ListOfPublication = em.createQuery("SELECT c FROM Book c").getResultList();

        Book c3 = ListOfPublication.get(no - 1);
        if (no - 1 > ListOfPublication.size() - 1) {
            System.out.println("Input Not valid.");
        } else {
            em.getTransaction().begin();
            Publication c4 = ListOfPublication.get(no - 1);
            em.remove(c4);

            em.getTransaction().commit();
        }
    }
}
